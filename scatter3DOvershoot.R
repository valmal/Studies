#Trying something new
#Install the required packages
install.packages("quantreg", type="mac.binary.mavericks")
install.packages("rgl",type="mac.binary.mavericks")
install.packages("car",type="mac.binary.mavericks")
install.packages("SparseM")

library(rkdb)
library(ggplot2)
library(plotly)
library(readtext)
library(car)
library(quantreg)
library(rgl)
credentials <- readtext("/Users/valeriyamalenko/q/details.txt", text_field = "texts")
h<-open_connection("localhost",4888,credentials[1,2])
execute(h, "data: get `$\":/Users/valeriyamalenko/Documents/speedSwitchResults\";")
data<-execute(h, "select from data where controlDelay=2")

# 3D plot without regression plane
scatter3d( y=data$kP,
           x=data$kI,
           z=data$overshoot, xlab = "kP", ylab = "kI",
           zlab = "Overshoot", point.col = "blue", 
           surface=FALSE, 
           axis.ticks=TRUE)

#Multiple linear regression for overshoot
fit<-lm(overshoot~kP+kI, data=data)
summary(fit)

# 3D plot with the regression plane - linear model
scatter3d( y=data2$overshoot,
           x=data2$kI,
           z=data2$kP, xlab = "kP", ylab = "kI",
           zlab = "Overshoot", point.col = "blue", grid=FALSE, fit="linear",
           model.summary=TRUE)

# 3D plot with the regression plane - quadratic model
scatter3d( y=data2$overshoot,
           x=data2$kI,
           z=data2$kP, xlab = "kP", ylab = "kI",
           zlab = "Overshoot", point.col = "blue", grid=FALSE, fit="quadratic",
           model.summary=TRUE)


#3D plot with the regression plane - smoooth model ==> the best
scatter3d( y=data$overshoot,
           x=data$kI,
           z=data$kP, xlab = "kP", ylab = "kI",
           zlab = "Overshoot", point.col = "blue", grid=FALSE, fit="smooth",
           model.summary=TRUE)

#Replicating this smooth regression
library(mgcv)
fit<-gam(data$overshoot~ s(data$kP,data$kI), family = gaussian(link="identity"), data, model = TRUE)
summary(fit)

#Finding outliers - an attempted method that I didn't use in the end
cooksd <- cooks.distance(fit1)
plot(cooksd, pch="*", cex=2, main="Influential Obs by Cooks distance") 
abline(h = 4*mean(cooksd, na.rm=T), col="red")
influential <- as.numeric(names(cooksd)[(cooksd > 4*mean(cooksd, na.rm=T))])
data[influential, ]
